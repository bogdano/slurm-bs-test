
class jurt {
	include util

	package {'jurt':
		ensure => installed }

	file {'/etc/jurt/jurt.conf':
		ensure => present,
		mode => '644',
		owner => 'root',
		group => 'root',
		source => 'puppet:///modules/jurt/jurt.conf',
		require => Package['jurt'] }

	file {'/etc/sudoers':
		ensure => present }

	util::line {'disable-notty':
		file => '/etc/sudoers',
		line => 'Defaults    requiretty',
		ensure => 'absent' }

	util::line {'enable-jurt-sudo':
		file => '/etc/sudoers',
		line => '%jurt ALL=(ALL) NOPASSWD: /usr/sbin/jurt-root-command',
		ensure => present }
}
