class slurm {

	class client {

		include munge

		package { "slurm":
			ensure => installed }

		user { 'slurm':
			ensure  => 'present',
			comment => 'SATAn been here',
			gid     => '555',
			home    => '/',
			shell   => '/sbin/nologin',
			uid     => '555',
		}

		group { 'slurm':
			ensure => 'present',
			gid    => '555',
		}

		file { '/etc/slurm/slurm.conf':
			ensure  => 'file',
			group   => '555',
			mode    => '644',
			owner   => '0',
			notify  => [Service["slurm"]],
			source  => 'puppet:///modules/slurm/slurm.conf'
		}

		service { "slurm":
			ensure => running,
			hasstatus => false,
			hasrestart => true,
			require => Package["slurm"] }

	}

	class server {
		include munge
		include client

		package { "slurm-slurmctld":
			ensure => installed }

		package { "slurm-auth-munge":
			ensure => installed }

		service { "slurmctld":
			ensure => running,
			hasstatus => false,
			hasrestart => true,
			require => Package["slurm-slurmctld"],
			subscribe => [File["/etc/slurm/slurm.conf"], Service["munge"]] }

		Service["munge"] -> Service["slurmctld"]
	}
}

