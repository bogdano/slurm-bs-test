
class repsys {
	package {'repsys':
		ensure => installed }

	file {'/etc/repsys.conf':
		ensure => present,
		owner => '0',
		mode => '644',
		source => 'puppet:///modules/repsys/repsys.conf' }

}
