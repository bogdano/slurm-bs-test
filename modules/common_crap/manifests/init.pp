class common_crap {
	file {'/etc/hosts':
		ensure => present,
		owner => '0',
		group => '0',
		mode => '644',
		source => 'puppet:///modules/common_crap/hosts'
	}
}
