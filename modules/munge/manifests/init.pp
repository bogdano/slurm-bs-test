class munge {

	package { "munge":
		ensure => installed }

	service { "munge":
		ensure => running,
		enable => true,
		require => Package["munge"] }

	file { '/etc/munge':
		ensure => 'directory',
		group  => 'munge',
		mode   => '700',
		owner  => 'munge',
		require => Package["munge"]
	}

	file { '/etc/munge/munge.key':
		ensure  => 'file',
		group   => 'munge',
		mode    => '400',
		owner   => 'munge',
		source  => 'puppet:///modules/munge/munge.key',
		require => Package["munge"]
	}

	File["/etc/munge"] -> File["/etc/munge/munge.key"]
}
