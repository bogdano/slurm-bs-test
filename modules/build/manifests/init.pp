
class build {

	class common {

		user { 'builder':
			ensure => 'present',
			home   => '/home/builder',
			shell  => '/bin/bash',
		}

		file { '/build-scripts':
			ensure => directory,
			recurse => true,
			mode => 755,
			owner => 'builder',
			source =>  'puppet:///modules/build/build-scripts'}
	}

	class builder {
		include jurt

		group { 'jurt':
			members => ['builder'] }


		file { '/build-export':
			ensure => directory,
			mode => 777,
			owner => 'builder' }

		package {'apache-mpm-prefork':
			ensure => present }
		package {'wget':
			ensure => present }

		service {'httpd':
			ensure => running,
			enable => true,
			require => Package["apache-mpm-prefork"] }

		file { '/etc/httpd/conf.d/90_build_export.conf':
			ensure => present,
			mode => 644,
			owner => 'root',
			source => 'puppet:///modules/build/90_build_export.conf',
			require => Package["apache-mpm-prefork"],
			notify => Service["httpd"] } #FIXME better apache name?

	}

	class repository_manager inherits common { }
}
