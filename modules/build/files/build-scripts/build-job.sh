#!/bin/bash -x
#SBATCH -o output.build.%j.%N.txt
#SBATCH --job-name submit
#SBATCH -p x86_64
# expects SUBMIT_PKGNAMES SUBMIT_TARGET SUBMIT_USER

echo "Submit user: $SUBMIT_USER"

EXPORTROOT=/build-export
SRPMDIR=`TMPDIR= mktemp -d -p "$EXPORTROOT" "submit.$SLURM_JOBID.$SUBMIT_USER.XXXXXXXXXX"`
SRPMDIRNAME=`basename $SRPMDIR`

for pkgname in $SUBMIT_PKGNAMES; do
	repsys getsrpm -t $SRPMDIR $pkgname
done

chmod 755 "$SRPMDIR"

baseurl=http://`hostname -s`/export/$SRPMDIRNAME

dependencies=""
srpms="$(cd $SRPMDIR; echo *.src.rpm)"
for arch in i586 x86_64; do

	BUILD_ID=$SRPMDIRNAME \
	PARENT_JOB_ID=$SLURM_JOB_ID \
	BUILD_ARCH=$arch \
	BUILD_TARGET="$SUBMIT_TARGET" \
	BUILD_SRPM_BASEURL="$baseurl" \
	BUILD_SRPMS="$srpms" \
		sbatch -p $arch /build-scripts/build-arch.sh
done
