#!/bin/bash -x
#SBATCH -o output.collect.%j.%N.txt
# expects BUILD_RPMS_URL BUILD_ID BUILD_ARCH

. /build-scripts/config.sh

TARGET_DIR="$HOME/builds/$BUILD_ID"
ARCH_DIR="$TARGET_DIR/$BUILD_ARCH"

[ ! -d "$TARGET_DIR" ] && mkdir -p "$TARGET_DIR"

DOWNLOAD_DIR=$(TMPDIR= mktemp -d -p "$TARGET_DIR" tmp.$BUILD_ARCH.XXXXXXX)

wget -nc \
	--cut-dirs 2 \
	-P "$DOWNLOAD_DIR" \
	--no-host-directories \
	-r --no-parent \
	--reject "index.html*,robots.txt" \
	"$BUILD_RPMS_URL"

mv "$DOWNLOAD_DIR" "$ARCH_DIR"

missing=
for arch in $REQUIRED_ARCHS; do
	[ ! -d "$TARGET_DIR/$arch" ] && missing="$arch $missing"
done

if [ -n "$missing" ]; then
	echo "Architectures still missing: $missing"
else
	echo "I would upload the packages to the target repository now"
fi
