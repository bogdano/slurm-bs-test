#!/bin/bash -x
#SBATCH -o output.arch.%j.%N.txt
#SBATCH -J build
# expects BUILD_ID BUILD_ARCH BUILD_TARGET BUILD_SRPM_BASEURL BUILD_SRPMS PARENT_JOB_ID

JURT_BUILD_ID=$BUILD_ID.$SLURM_JOBID
BUILD_RPMS_URL=http://`hostname -s`/export/$JURT_BUILD_ID/

BUILD_RPMS_URL="$BUILD_RPMS_URL" \
BUILD_ID=$BUILD_ID \
BUILD_ARCH=$BUILD_ARCH \
	sbatch -d afterok:$SLURM_JOBID \
		-p repo \
		/build-scripts/collect.sh

TMPDIR=`mktemp -d`

cd $TMPDIR
for srpm in $BUILD_SRPMS; do
	echo "srpm: $srpm"
	url=$BUILD_SRPM_BASEURL/$srpm
	wget "$url"
done

jurt-build --showlog -n $JURT_BUILD_ID \
	-t $BUILD_TARGET-$BUILD_ARCH \
	*.src.rpm

cp -af ~/jurt/$JURT_BUILD_ID /build-export/$JURT_BUILD_ID

rm -rf --one-file-system --preserve-root "$TMPDIR"

cd ~

find /build-export/$JURT_BUILD_ID -type d | xargs chmod 0755
date
