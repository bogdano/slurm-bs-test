class buildnode {
	include common_crap
	include slurm::client
	include build::builder
	include jurt
	include repsys
}

class repomaster {
	include common_crap
	include slurm::client
	include build::repository_manager
}

##################################################

node tcc159 {
	include slurm::server
	include buildnode
}

node lviv {
	include buildnode
}

node dnipro {
	include buildnode
}

node tcc149 {
	include repomaster
}
